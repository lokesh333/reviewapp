import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavService } from '../Shared/services/navigation/nav.service';

@Component({
  selector: 'app-review-app-layout',
  templateUrl: './review-app-layout.component.html',
  styleUrls: ['./review-app-layout.component.scss']
})
export class ReviewAppLayoutComponent implements OnInit {
  public data: string[] = [];
  public loggedInuserDetails: any = [];

  constructor(private router: Router, private navService: NavService) { }

  ngOnInit() {
    // this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));

    this.navService.getSidebar().subscribe((response) => {
      let finalArray = [];
      let arr = [];
      if (response && response['menu'] && response['menu'].length > 0) {
        response["menu"][0]['subMenu'].forEach(menus => {
          // if (menus.Role.some(x => x === this.loggedInuserDetails.Role) && menus.Level.some(x => x === this.loggedInuserDetails.Level) || menus.userType.some(x => x === this.loggedInuserDetails.UserType)) {
            arr.push(menus);
          // }
        });
        finalArray.push({ link: response["menu"][0].link, name: response["menu"][0].name, subMenu: arr });
        this.data = finalArray;

      }
    });
  }
}
