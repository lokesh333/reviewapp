import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IProduct } from 'src/app/Shared/Modals/review.model';
import { Subscription } from 'rxjs';
import { ReviewService } from 'src/app/Shared/Services/review.service';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  @ViewChild('PopUpModal', { static: false }) public PopUpModal: ModalDirective;
  @ViewChild('EmpForm', { static: false }) public EmpForm: any;
  @ViewChild('modalConfirmDelete', { static: false }) public modalConfirmDelete: ModalDirective;
  public loggedInuserDetails: any = [];
  public ProductModel: IProduct = <IProduct>{};
  public SelectProdSubscription: Subscription;
  public InsertProdSubscription: Subscription;
  CategoryList: any;
  SubCategoryList: any;
  successmsg: any;
  unsuccessmsg: any;
  PopupName: string;
  DeleteInput: any;
  SubCategoryListFixed: any;
  constructor(private reviewService: ReviewService, public _router: Router) { }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails ? this.loggedInuserDetails[0] : this.loggedInuserDetails;
    this.ProductSelect('CategorySelect');
    this.ProductSelect('SubCategorySelect');
  }

  ProductInsert(Model, input) {
    this.SelectProdSubscription = this.reviewService.AuthInsert(Model, this.loggedInuserDetails.Phone, null,null, input).subscribe((response) => {
      if (response && response["data"]) {
        switch (input) {
          case 'Add Category':
            this.successmsg = response["data"][0]['resp'];
            this.ProductModel.CategoryName = null;
            this.ProductSelect('CategorySelect');
            this.ProductSelect('SubCategorySelect');
            this.EmpForm.reset();
            break;
          case 'Add SubCategory':
            this.successmsg = response["data"][0]['resp'];
            this.ProductSelect('SubCategorySelect');
            this.EmpForm.reset();
            break;
          case 'Update Category':
            this.successmsg = response["data"][0]['resp'];
            this.ProductModel.CategoryName = null;
            this.ProductSelect('CategorySelect');
            this.ProductSelect('SubCategorySelect');
            this.EmpForm.reset();
            break;
          case 'Update SubCategory':
            this.successmsg = response["data"][0]['resp'];
            this.ProductSelect('SubCategorySelect');
            this.EmpForm.reset();
            break;
          case 'Delete Category':
            this.successmsg = response["data"][0]['resp'];
            this.ProductSelect('CategorySelect');
            this.ProductSelect('SubCategorySelect');
            this.modalConfirmDelete.hide();
            break;
          case 'Delete SubCategory':
            this.successmsg = response["data"][0]['resp'];
            this.ProductSelect('SubCategorySelect');
            this.modalConfirmDelete.hide();
            break;

        }
      }
    });
  }


  ProductSelect(input) {
    this.SelectProdSubscription = this.reviewService.ProductSelect(input, this.loggedInuserDetails ? this.loggedInuserDetails.Phone : null, this.ProductModel.ProductId ? this.ProductModel.ProductId : null, null, this.ProductModel.Name).subscribe((response) => {
      if (response && response["data"]) {
        switch (input) {
          case 'CategorySelect':
            this.CategoryList = response['data'];
            break;
          case 'SubCategorySelect':
            this.SubCategoryList = response['data'];
            this.SubCategoryListFixed = response['data'];
            break;
        }
      }
    });
  }

  popup(model, input) {
    this.PopUpModal.show();
    switch (input) {
      case 'Add Category':
        this.PopupName = "Add Category";
        break;
      case 'Add SubCategory':
        this.PopupName = "Add SubCategory";
        break;
      case 'Update Category':
        this.PopupName = "Update Category";
        this.ProductModel.CategoryName = model.CategoryName;
        this.ProductModel.Id = model.Id;
        break;
      case 'Update SubCategory':
        this.PopupName = "Update SubCategory";       
        this.ProductModel.CategoryName = model.CategoryName;
        this.ProductModel.Id = model.Id;
        this.ProductModel.SubCategoryName = model.SubCategoryName;
        this.ProductModel.CategoryId = model.CategoryId;

        break;
    }
  }

  filter(Id){
    if(Id==0){
      this.SubCategoryList=this.SubCategoryListFixed;
    }
    else{
      this.SubCategoryList=this.SubCategoryListFixed.filter(x=>x['CategoryId']===Id)
    }   
  }

  deletepopup(model, input) {
    this.ProductModel.Id = model.Id;
    this.DeleteInput = input;
    this.modalConfirmDelete.show();
  }


  closePopUpModal() {
    this.PopUpModal.hide();
  }

  // ******* Successfully and unsuccessfully message hide click on screen ********//
  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }

  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.SelectProdSubscription ? this.SelectProdSubscription.unsubscribe() : null;
    this.InsertProdSubscription ? this.InsertProdSubscription.unsubscribe() : null;
  }

}
