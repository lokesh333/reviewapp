import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { IProduct } from 'src/app/Shared/Modals/review.model';
import { Subscription } from 'rxjs';
import { ReviewService } from 'src/app/Shared/Services/review.service';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {
  @ViewChild('PopUpModal', { static: false }) public PopUpModal: ModalDirective;
  @ViewChild('scrollMe', { static: false }) public scrollMe: ElementRef;
  @ViewChild('modalConfirmDelete', { static: false }) public modalConfirmDelete: ModalDirective;
  public loggedInuserDetails: any = [];
  public ProductModel: IProduct = <IProduct>{};
  public SelectProdSubscription: Subscription;
  public InsertProdSubscription: Subscription;
  public unsuccessmsg: string;
  public successmsg: string;
  public Page: string;
  public CategoryList: string[];
  public ProductList: any;
  public MyProductList: string[];
  public rate: boolean[] = [];
  public RightView: string;
  public CommentList: string[];
  public filter2: any;
  public SubCategories: any;
  public ProductChecked: any = [];
  public CheckedProducts: any;
  public ImagesList: string[];
  public index: number;
  public SubCategoryList: string[];
  public ProdCategoryList: Set<unknown>;
  public ProductListFixed: string[];
  public Language: string;
  constructor(private reviewService: ReviewService, public _router: Router) {
    this.Page = "Home";
  }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails ? this.loggedInuserDetails[0] : null;
    this.ProductSelect('ProductSelect');
    this.ProductSelect('CategorySelect');
    this.ProductSelect('SubCategorySelect');
    this.rate[0] = false;
    this.Page = "Home";
    this.Language = "English";
  }

  ProductSelect(input) {
    this.SelectProdSubscription = this.reviewService.ProductSelect(input, this.loggedInuserDetails ? this.loggedInuserDetails.Phone : null, this.ProductModel.ProductId ? this.ProductModel.ProductId : null, this.CheckedProducts, this.ProductModel.Name).subscribe((response) => {
      if (response && response["data"]) {
        switch (input) {
          case 'CategorySelect':
            this.CategoryList = response['data'];
            break;
          case 'SubCategorySelect':
            this.SubCategoryList = response['data'];
            break;
          case 'ProductSelect':
            this.ProductListFixed = response['data'];
            this.ProductList = response['data'];
            this.ProdCategoryList = new Set(this.ProductList.map((e) => { return e.CategoryName }))
            break;
          case 'MyProductSelect':
            this.MyProductList = response['data'];
            break;
          case 'OneProductSelect':
            this.RightView = 'Comments';
            this.ProductSelect("ProductRating");
            this.ProductSelect('SelectProductComments');
            break;
          case 'Comments':
            this.RightView = 'Comments';
            this.ProductSelect('SelectProductComments');
            break;
          case 'Details':
            this.RightView = 'Details';
            break;
          case 'SelectProductComments':
            this.CommentList = response['data'];
            break;
          case 'ProductRating':
            let num = response['data'][0] ? response['data'][0]['UserRating'] : 0;
            for (let i = 1; i <= num; i++) {
              this.rate[i] = true;
            }
            for (let i = num + 1; i <= 5; i++) {
              this.rate[i] = false;
            }
            break;
          case 'MultiProductSelect':
            this.ProductList = response['data'];
            break;

            break;
          case 'MultiProductSearch':
            this.ProductList = response['data'];
            break;
        }
      }
    });
  }

  ProductInsert(input) {
    if (!this.loggedInuserDetails) {
      this._router.navigate(['/login/']);
    }
    else {
      this.InsertProdSubscription = this.reviewService.AuthInsert(this.ProductModel, this.loggedInuserDetails.Phone, null, null, input).subscribe((response) => {
        if (response && response["data"]) {
          switch (input) {
            case 'InsertRating':
              break;
            case 'InsertFileComments':
              this.ProductModel.Review = null;
              this.ProductSelect('SelectProductComments');
              break;
            case 'DeleteReview':
              this.ProductSelect('SelectProductComments');
              this.modalConfirmDelete.hide();
              break;
          }

        }
      });
    }
  }

  filter(input) {

    this.SubCategories = this.SubCategoryList ? this.SubCategoryList.filter(x => x['CategoryName'] === input) : null
    this.ProductList = this.ProductListFixed ? this.ProductListFixed.filter(x => x['CategoryName'] === input) : null
    this.ProductChecked.filter(x => x['checked'] === true).map(item => { item['checked'] = false });
  }


  checkbox(model) {
    if (model.checked) {
      this.ProductChecked.push(model);
    }
    else {
      let index = this.ProductChecked.indexOf(model);
      if (index !== -1) {
        this.ProductChecked.splice(index, 1);
      }
    }
    let arr = this.ProductChecked.map((e) => { return e.SubCategoryName })
    this.CheckedProducts = arr.join(',');
    this.ProductSelect("MultiProductSelect");
  }


  clearAllFilter() {
    this.ProductSelect('ProductSelect');
    this.ProductChecked.filter(x => x['checked'] === true).map(item => { item['checked'] = false });
    this.ProductChecked = [];
    this.SubCategories = [];
    this.filter2 = null;
  }


  openProduct(model) {
    this.ProductModel = model;
    this.Page = "ProductView";
    this.index = 0
    this.ImagesList = new Array(model.DisplayImage);
    var array = model.ProductImages ? model.ProductImages.split(',') : null;
    for (let i in array) {
      this.ImagesList.push(array[i]);
    }
    this.ProductSelect('OneProductSelect');
  }

  imageslide(input) {
    switch (input) {
      case 'left':
        if (this.index > 0)
          this.index = this.index - 1;
        break;
      case 'right':
        if (this.index < this.ImagesList.length - 1)
          this.index = this.index + 1;
        break;
    }
  }


  scrollToBottom = () => {
    try {
      this.scrollMe.nativeElement.scrollTop = this.scrollMe.nativeElement.scrollHeight;
    } catch (err) { }
  }


  openRightView(input) {
    this.ProductSelect(input);
  }


  ratingChange(number) {
    for (let i = 1; i <= number; i++) {
      this.rate[i] = true;
    }
    for (let i = number + 1; i <= 5; i++) {
      this.rate[i] = false;
    }
    this.ProductModel.Rate = number;
    this.ProductInsert('InsertRating');
  }

  settings() {
    this._router.navigate(['/review/settings/']);
  }

  onKeydown(event) {
    this.ProductSelect('MultiProductSearch');
  }

  deleteComment(Id) {
    this.modalConfirmDelete.show();
    this.ProductModel.Id = Id;
  }

  closePopUpModal() {
    this.PopUpModal.hide();
  }

  myProducts() {
    if (!this.loggedInuserDetails) {
      this._router.navigate(['/login/']);
    }
    else {
      this.Page = "MyProducts";
      this.ProductSelect('MyProductSelect');
    }
  }

  logout() {
    localStorage.removeItem('isLoggedin');
    localStorage.removeItem('LoggedinUser');
    this._router.navigate(['/review/product']);
    localStorage.clear();
    this.loggedInuserDetails = [];
    this.ngOnInit();
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }

  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.SelectProdSubscription ? this.SelectProdSubscription.unsubscribe() : null;
    this.InsertProdSubscription ? this.InsertProdSubscription.unsubscribe() : null;
  }

}
