import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewAppLayoutComponent } from './review-app-layout.component';
const routes: Routes = [
  {
    path: '',
    component: ReviewAppLayoutComponent,
    children: [
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
      },
      {
        path: 'user',
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
      }
      ,
      {
        path: 'sell',
        loadChildren: () => import('./sell/sell.module').then(m => m.SellModule),
      }
      ,
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewAppLayoutRoutingModule { }
