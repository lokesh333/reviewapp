import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IProduct } from 'src/app/Shared/Modals/review.model';
import { Subscription } from 'rxjs';
import { ReviewService } from 'src/app/Shared/Services/review.service';
import { FileAttachment } from 'src/app/Shared/Components/file-upload/file-attachment.model';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.scss']
})
export class SellComponent implements OnInit {
  public loggedInuserDetails: any = [];
  public ProductModel: IProduct = <IProduct>{};
  public SelectProdSubscription: Subscription;
  public InsertProdSubscription: Subscription;
  public uploadImageFile: FileAttachment[] = [];
  public uploadImageFile2: FileAttachment[] = [];
  public unsuccessmsg: string;
  public successmsg: string;
  public CategoryList: any;
  public PropertyList: any;
  public SubCategoryList: any;
  public SubCategoryListTemp: any;
  public Language: string;
  constructor(private reviewService: ReviewService, public _router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {

    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails ? this.loggedInuserDetails[0] : this.loggedInuserDetails;
    this.ProductSelect('CategorySelect');
    this.ProductSelect('SubCategorySelect');
    this.Language="English";
  }

  ProductInsert(Model, input) {
    this.SelectProdSubscription = this.reviewService.AuthInsert(Model, this.loggedInuserDetails.Phone, this.uploadImageFile, this.uploadImageFile2, input).subscribe((response) => {
      if (response && response["data"]) {
        switch (input) {
          case 'ProductInsert':
            this.successmsg = response["data"][0]['resp'];
            this.ProductModel.Address = null;
            this.ProductModel.Cost = null;
            this.ProductModel.Description = null;
            this.ProductModel.ProductName = null;
            this.ProductModel.SubCategoryName = null;
            this.uploadImageFile = [];
            this.uploadImageFile2 = [];
            break;
        }
      }
    });
  }

  ProductSelect(input) {
    this.SelectProdSubscription = this.reviewService.ProductSelect(input, this.loggedInuserDetails.Phone, null, null, null).subscribe((response) => {
      if (response && response["data"]) {
        switch (input) {
          case 'CategorySelect':
            this.CategoryList = response['data'];
            break;
          case 'SubCategorySelect':
            this.SubCategoryList = response['data'];
            break;
        }
      }
    });
  }

  categoryChange(Id) {
    Id = +Id;
    this.SubCategoryListTemp = this.SubCategoryList.filter(x => x['CategoryId'] === Id)
  }

  location() {

  }

  products() {
    this._router.navigate(['/review/product/']);
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }

  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.SelectProdSubscription ? this.SelectProdSubscription.unsubscribe() : null;
    this.InsertProdSubscription ? this.InsertProdSubscription.unsubscribe() : null;
  }



}
