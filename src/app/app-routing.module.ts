import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: '', loadChildren: () => import('src/app/review-app-layout/review-app-layout.module').then(m => m.ReviewAppLayoutModule) },
  { path: '', loadChildren: () => import('src/app/review-app-layout/product/product.module').then(m => m.ProductModule) },
  { path: 'login', loadChildren: () => import('src/app/authentication/login/login.module').then(m => m.LoginModule) },
  { path: 'review', loadChildren: () => import('src/app/review-app-layout/review-app-layout.module').then(m => m.ReviewAppLayoutModule) },
  { path: 'sell', loadChildren: () => import('src/app/review-app-layout/sell/sell.module').then(m => m.SellModule) },
  { path: 'settings', loadChildren: () => import('src/app/review-app-layout/settings/settings.module').then(m => m.SettingsModule) }
  // { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }