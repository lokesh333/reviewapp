import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IAuth } from 'src/app/Shared/Modals/review.model';
import { Subscription } from 'rxjs';
import { ReviewService } from 'src/app/Shared/Services/review.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public SignUpPage: boolean;
  public SignInPage: string;
  public SelectAuthSubscription: Subscription;
  public InsertAuthSubscription: Subscription;
  public successmsg: string;
  public unsuccessmsg: string;
  public Session: any;
  public Language: string="English";
  constructor(private reviewService: ReviewService, public _router: Router) { }
  public AuthModel: IAuth = <IAuth>{};

  ngOnInit() {
    const signUpButton = document.getElementById('signUp');
    const signInButton = document.getElementById('signIn');
    const container = document.getElementById('container');
    this.Language="English";
    signUpButton.addEventListener('click', () => {
      container.classList.add("right-panel-active");
      this.SignUpPage = true;
    });
    signInButton.addEventListener('click', () => {
      container.classList.remove("right-panel-active");
    });
    this.SignInPage = "SignIn";
  }

  signUp() {
    this.SelectAuthSubscription = this.reviewService.getOtp(this.AuthModel.Phone).subscribe((response) => {
      if (response && response.toString().substr(0, 5) != "Error" && JSON.parse(response.toString())['Status'] == 'Success') {
        this.Session = JSON.parse(response.toString())['Details'];
        this.SignUpPage = false;
      } else {
        this.successmsg = "Wrong Number";
      }
    });
  }

  back() {
    this._router.navigate(['/review/product/']);
  }

  verify() {
    this.SelectAuthSubscription = this.reviewService.verify(this.Session, this.AuthModel.Otp).subscribe((response) => {
      if (JSON.parse(response.toString())['Details'] === 'OTP Matched') {
        this.InsertAuthSubscription = this.reviewService.AuthInsert(this.AuthModel, null, null, null, 'UserInsert').subscribe((response) => {
          if (response && response["successful"]) {
            this.successmsg = response['data'][0]['Resp'];
            setTimeout(function () {
              document.getElementById('container').classList.remove("right-panel-active");
            }, 1000);
          }
        });
      } else {
        this.successmsg = "OTP Mismatched";
      }
    });
  }

  loginpage(model) {
    this.SelectAuthSubscription = this.reviewService.AuthInsert(model, null, null, null, 'Login').subscribe((response) => {
      if (response && response["successful"]) {
        if (response['data'][0].UserType == 'Admin' || response['data'][0].UserType == 'Basic') {
          localStorage.setItem('currentUser', JSON.stringify(response['data']));
          this._router.navigate(['/review/product/']);
        } else {
          this.successmsg=response['data'][0].Resp;
        }
      } else {
      }
    });
  }

  forgotPwd() {
    this.SignInPage = 'Forgot';
  }

  getOtpforgot() {
    this.SelectAuthSubscription = this.reviewService.getOtp(this.AuthModel.Phone).subscribe((response) => {
      if (response && response.toString().substr(0, 5) != "Error" && JSON.parse(response.toString())['Status'] == 'Success') {
        this.Session = JSON.parse(response.toString())['Details'];
        this.SignInPage = 'VerifyForgotNum';
      } else {
        this.successmsg = "Wrong Number";
      }
    });
  }

  verifyOtpForgot() {
    this.SelectAuthSubscription = this.reviewService.verify(this.Session, this.AuthModel.Otp).subscribe((response) => {
      if (JSON.parse(response.toString())['Details'] === 'OTP Matched') {
        this.SignInPage = 'New PWD';
      }
      else {
        this.successmsg = "OTP Mismatched";
      }
    });
  }

  changePwd() {
    this.SelectAuthSubscription = this.reviewService.AuthInsert(this.AuthModel, null, null, null, 'Change PWD').subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = "PassWord Changed"
        this.SignInPage = "SignIn";
      } else {
      }
    });
  }

  // ******* Successfully and unsuccessfully message hide click on screen ********//

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }

  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.SelectAuthSubscription ? this.SelectAuthSubscription.unsubscribe() : null;
    this.InsertAuthSubscription ? this.InsertAuthSubscription.unsubscribe() : null;
  }




}
