import { Component, OnInit, Input, SimpleChanges, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { NavItem } from '../navigations/nav.item';
import * as $ from 'jquery';
interface Page {
  link: string;
  name: string;
  icon: string;
}

@Component({
  selector: 'tru-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})

export class SidemenuComponent implements OnInit {
  public loggedInuserDetails: any =[]
  @Input() data: any;
  public sideNavState: boolean = false;
  public linkText: boolean = false;
  public topMenu: any;
  public navItems: any;
  public MenuList: any;
  public SubMenuList: any;
  public hideAll: any;
  public showrole: string;
  public pages: Page[] = [
    { name: 'Inbox', link: 'some-link', icon: 'inbox' },
    { name: 'Starred', link: 'some-link', icon: 'star' },
    { name: 'Send email', link: 'some-link', icon: 'send' }]

  constructor(public router: Router) { }

  ngOnInit() {
    // this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    // this.getRoleAsString(this.loggedInuserDetails["UserType"], this.loggedInuserDetails["Role"]);
    this.MenuList = this.data;
    this.close();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && this.data && this.data.length > 0) {
      this.MenuList = this.data;
      if (changes.data.currentValue.length !== 0) {  // for initially load the first selected Item
        this.topMenu = this.data[0].displayName;
        if (this.data.length > 0)
          this.navItems = this.data;
        else if (this.data[0].length > 0)
          this.navItems = this.data[0]["menu"][0]['menu'];
        else
          this.router.navigate(['/login']);                 // If navItems is undefined or no Access for that user route to Login
      }
    }
  }

  //Method for View Submenu Items
  submenu = (item): void => {
    if (item && item !== '') {
      this.SubMenuList = item["subMenu"];
    }
  }
  // Select Menu Item
  onItemSelected = (item: NavItem): void => {
    if (!item.children || !item.children.length) {
      this.router.navigate([item.route]);
    }
  }

  //Method for logging out
  onLoggedOut = (): void => {
    localStorage.removeItem('isLoggedin');
    localStorage.removeItem('LoggedinUser');
    this.router.navigate(['/login']);
    localStorage.clear();
  }

  //Used for NgFor to unique Id Everytime
  trackByFn(item) {
    return item.id;
  }

  //Close SideMenu with Close-sidebar class
  closesidebar() {
    $("#close-sidebar").click(function () {
      $(".page-wrapper").removeClass("toggled");
    });
  }

  // Open SideMenu
  Opensidebar() {
    $("#show-sidebar").click(function () {
      $(".page-wrapper").addClass("toggled");
    });
  }

  // Close SideMenu
  close() {
    setTimeout(() => {
      $(".page-wrapper").removeClass("toggled");
    }, 200);
  }

  getRoleAsString = (Usertype, role): void => {
    switch (Usertype.toLowerCase()) {
      case "employee": {
        switch (role) {
          case 1:
            { this.showrole = "Admin"; break; }
          case 2:
            { this.showrole = "CRM"; break; }
          case 3:
            { this.showrole = "Employee"; break; }
          case 4:
            { this.showrole = "New Employee"; break; }
          case 5:
            { this.showrole = "Estimation"; break; }
        }
        break;
      }
    }
  }
}
