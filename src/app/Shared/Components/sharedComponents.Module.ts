import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NavigationsComponent } from './navigations/navigations.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxPaginationModule } from 'ngx-pagination';
import { TablePaginationComponent } from './table-pagination/table-pagination.component';
import { MaterialModule } from 'src/app/material-module';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

@NgModule({
    declarations: [
        NavigationsComponent,
        TablePaginationComponent,SidemenuComponent,FileUploadComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule,
        FormsModule,
        MDBBootstrapModule.forRoot(),      
        NgxPaginationModule,
    ],
    exports: [
        NavigationsComponent,
        TablePaginationComponent,SidemenuComponent,FileUploadComponent
    ]
})
export class SharedComponentsModule { }