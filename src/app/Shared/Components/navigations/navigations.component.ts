import { Component, ViewChild, ElementRef, Input, SimpleChanges, OnChanges } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { NavItem } from './nav.item';
import * as $ from 'jquery';
import { NavService } from '../../services/navigation/nav.service';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { ReviewService } from 'src/app/Shared/Services/review.service';

@Component({
  selector: 'tru-navigations',
  templateUrl: './navigations.component.html',
  styleUrls: ['./navigations.component.scss'],
})
export class NavigationsComponent implements OnChanges {
  public SelectProdSubscription: Subscription;
  public InsertProdSubscription: Subscription;
  public loggedInuserDetails: any = [];
  public mobileQuery: MediaQueryList;
  public navItems = [];
  public topMenu = [];
  public enableHRMS: boolean = true;
  public showrole: string = '';
  public SubMenuItems: any[];
  // public data3: any[];
  public employeeProfile: string = '';
  public MenuItems: any[] = [];

  title = 'angular-theme';
  appId = 'theme1';

  @ViewChild('appDrawer', { static: false }) appDrawer: ElementRef;
  @Input() data: any;
  @Input() sidenav: MatSidenav;

  public isHandset$: Observable<boolean> = this._breakpointObserver.observe(Breakpoints.Handset).pipe(map(result => result.matches));

  Materials: any;
  Apartments: any;


  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _navService: NavService,
    public _router: Router,
    private reviewService: ReviewService) {
    
  }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails ? this.loggedInuserDetails[0] : this.loggedInuserDetails;
    this._navService.appDrawer = this.appDrawer;
    setTimeout(() => {
      this.Opensidebar()
    }, 0);

  }


  sell() {
    if (this.loggedInuserDetails)
      this._router.navigate(['/review/sell/']);
    else
      this._router.navigate(['/login/']);
  }

  ngAfterViewInit() {
    this._navService.appDrawer = this.appDrawer;
    setTimeout(() => {
      this.Opensidebar()
    }, 0);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && this.data && this.data.length > 0) {
      if (changes.data.currentValue.length != 0) {           // for initially load the first selected Item
        this.topMenu = this.data[0].subMenu;
        if (this.data[0]["subMenu"].length > 0)
          this.navItems = this.data[0]["subMenu"][0]['subMenu'];
        else
          this._router.navigate(['/login']);                  // If navItems is undefined or no Access for that user route to Login
      }
    }
  }

  // Show MenuList
  showmenu() {
    this.MenuItems = this.data[0].subMenu;
    setTimeout(() => {
      $(".sidebar-dropdown > a").on('click touchstart', function () {
        //$(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(0);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(0);
        }
      });
    }, 0);
  }

  logout() {
    localStorage.removeItem('isLoggedin');
    localStorage.removeItem('LoggedinUser');
    this._router.navigate(['/review/product']);
    localStorage.clear();
    this.loggedInuserDetails=[];
    this.ngOnInit();
  }

  openSidebarOptions = (item): void => {
    if (item && item.subMenu) {
      this.navItems = item.subMenu;
    }
  }

  viewSubmenu(submenu) {
    this.SubMenuItems = this.data[0].subMenu.filter(x => x.displayName === submenu.displayName);
    // this.data3 = this.SubMenuItems[0].subMenu.displayName;
  }

  onItemSelected(item: NavItem): void {
    if (!item.children || !item.children.length) {
      this._router.navigate([item['subMenu'][0].route]);
    }
  }

  newside() {
    $("#leftside-navigation .sub-menu > a").click(function (e) {
      $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(),
        e.stopPropagation()
    })
  }

  // Logout Uuser
  onLoggedOut = (): void => {
    localStorage.removeItem('isLoggedin');
    localStorage.removeItem('LoggedinUser');
    this._router.navigate(['/login']);
    localStorage.clear();
  }

  switchTheme(appId: string): void {
    this.appId = appId;
  }

  //Open SideMenu
  Opensidebar() {
    $(".page-wrapper").addClass("toggled");
  }

  // Close SideMenu
  closesidebar() {
    $("#close-sidebar").click(function () {
      $(".page-wrapper").removeClass("toggled");
    });
  }

  getRoleAsString = (Usertype, role): void => {
    switch (Usertype.toLowerCase()) {
      case "employee": {
        switch (role) {
          case 1:
            { this.showrole = "Admin"; break; }
          case 2:
            { this.showrole = "CRM"; break; }
          case 3:
            { this.showrole = "Employee"; break; }
          case 4:
            { this.showrole = "New Employee"; break; }
          case 5:
            { this.showrole = "Estimation"; break; }
        }
        break;
      }
    }
  }

  //Used for NgFor to unique Id Everytym
  trackByFn(item) {
    return item.id;
  }
}

