export interface IAuth {
    UserName: string,
    Phone: number,
    Password: string,
    Otp: number
}

export interface IProduct {
    Id: number,
    ProductId: number,
    CategoryId: number,
    CategoryName: string,
    SubCategoryName: string,
    ProductName: string,
    Description: string,
    Address: string,
    Cost: number,
    ProductImages: string,
    DisplayImage: string,
    CreatedBy: string,
    CreatedOn: string,
    UserName: string,
    Rate: number,
    Quality: number,
    AvgRating: number,
    Review: string,
    Name: string,
    State: string,
    District: string,
    Town: string
}

