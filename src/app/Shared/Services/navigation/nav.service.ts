
import { Injectable } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavService {
  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);

  constructor(private router: Router, private http: HttpClient) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    this.appDrawer.open();
  }

  getSidebar = () => {
    const url = `./../../../../assets/Navigation/navigations.json`;
    return this.http.get(url).pipe(map(x => x));
  }

  // getNavigation = () => {
  //   const url = environment.apiUrl +`NavigationJsonGet`;
  //   return this.http.get(url).pipe(map(x => x));
  // }

}
