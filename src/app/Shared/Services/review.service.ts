import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {


  constructor(private http: HttpClient) { }

  AuthInsert = (Model, UserId, uploadImageFile, uploadImageFile2, Input) => {
    let data = {
      insertModel: {
        UserId: UserId ? UserId : null,
        Id: Model.Id ? Model.Id : null,
        ProductId: Model.ProductId ? Model.ProductId : null,
        CategoryId: Model.CategoryId ? Model.CategoryId : null,
        Rate: Model.Rate ? Model.Rate : null,
        Quality: Model.Quality ? Model.Quality : null,
        UserName: Model.UserName ? Model.UserName : null,
        Password: Model.Password ? Model.Password : null,
        Otp: Model.Otp ? Model.Otp : null,
        Phone: Model.Phone ? Model.Phone : null,
        ProductName: Model.ProductName ? Model.ProductName : null,
        ProductImages: null,
        DisplayImage: null,
        CategoryName: Model.CategoryName ? Model.CategoryName : null,
        SubCategoryName: Model.SubCategoryName ? Model.SubCategoryName : null,
        Description: Model.Description ? Model.Description : null,
        Address: Model.Address ? Model.Address : null,
        Cost: Model.Cost ? Model.Cost : null,
        Review: Model.Review ? Model.Review : null,
        Input: Input ? Input : null
      },
      File: uploadImageFile,
      File2: uploadImageFile2
    }
    const url = `${environment['hrApiUrl']}api/ReviewApp/ReviewAppAuthInsert`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }

  ProductSelect = (input, UserId, ProductId, CheckedProducts, Name) => {
    let data = {
      UserId:UserId?UserId:null,
      ProductId:ProductId?ProductId:null,
      CheckedProducts:CheckedProducts?CheckedProducts:null,
      Name:Name?Name:null,
      input:input?input:null
    }
    const url = `${environment['hrApiUrl']}api/ReviewApp/ReviewAppAuthSelect`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }

  getOtp = (Phone) => {
    const url = `https://salescustomerdevapi.azurewebsites.net/GetOtp/mobileNo?mobileNo=${Phone}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  verify = (session, otp) => {
    const url = `https://salescustomerdevapi.azurewebsites.net/VarifyOtp/mobileNo/OTP?sessionID=${session}&OTP=${otp}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  public jwt() {
    if (localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser'))) {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token
        })
      }
    }
  }
}


