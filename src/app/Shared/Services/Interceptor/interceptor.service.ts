import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const excludesArr = ['GetLoginDetails', 'https://file.io']
    if (!excludesArr.find(x => req.url.includes(x))) {
      const idToken = (localStorage && localStorage.getItem('currentUser')) ? JSON.parse(localStorage.getItem('currentUser')).token : null;
      if (idToken) {
        const cloned = req.clone({
          headers: req.headers.set("Authorization", "Bearer " + idToken)
        });
        return next.handle(cloned).pipe(<any>catchError(error => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('currentUser');
            this.router.navigate(['/login']);
          } else {
            return error;
          }
        }));
      } else {
        return next.handle(req);
      }
    }
  }
}
